(cons* (channel
        (name 'nonguix)
        (url "https://gitlab.com/nonguix/nonguix")
        ;; Enable signature verification:
        (introduction
         (make-channel-introduction
          "897c1a470da759236cc11798f4e0a5f7d4d59fbc"
          (openpgp-fingerprint
           "2A39 3FFF 68F4 EF7A 3D29  12AF 6F51 20A0 22FB B2D5"))))
       ;;       (channel
       ;;	(name 'dmbcs-development)
       ;;	(url "https://rdmp.org/dmbcs/guix-channel.git"))
       (channel
        (name 'flat)
        (url "https://github.com/flatwhatson/guix-channel.git")
        (introduction
         (make-channel-introduction
          "33f86a4b48205c0dc19d7c036c85393f0766f806"
          (openpgp-fingerprint
           "736A C00E 1254 378B A982  7AF6 9DBE 8265 81B6 4490"))))
       ;;       (channel
       ;;	(name 'ret)
       ;;	(url "https://github.com/UristMcKorobochka/ret-guix-channel.git")
       ;;	(introduction
       ;;	 (make-channel-introduction
       ;;	  "2fd888c15ffe1ffa8e0202d5ee85fa2fb7624f5d"
       ;;	  (openpgp-fingerprint
       ;;	   "411D 5521 01A3 A27A F3CB  00E7 6F90 2932 A5EC 0480"))))
       ;; (channel
       ;;  (name 'guix-gaming-games)
       ;;  (url "https://gitlab.com/guix-gaming-channels/games.git")
       ;;  ;; Enable signature verification:
       ;;  (introduction
       ;;   (make-channel-introduction
       ;;    "c23d64f1b8cc086659f8781b27ab6c7314c5cca5"
       ;;    (openpgp-fingerprint
       ;;     "50F3 3E2E 5B0C 3D90 0424  ABE8 9BDC F497 A4BB CC7F"))))
       ;; (channel
       ;;  (name 'guix-home-manager)
       ;;  (url "https://framagit.org/tyreunom/guix-home-manager.git")
       ;;  (introduction
       ;;    (make-channel-introduction
       ;;      "b5f32950a8fa9c05efb27a0014f9b336bb318d69"
       ;;      (openpgp-fingerprint
       ;;        "1EFB 0909 1F17 D28C CBF9  B13A 53D4 57B2 D636 EE82"))))
       %default-channels)
